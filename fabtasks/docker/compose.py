from fabric.api import *

@task
def build():
    local('sudo docker-compose build')

@task
def up():
    local('sudo docker-compose up -d')

@task
def down():
    local('sudo docker-compose down')

@task
def logs():
    local('sudo docker-compose logs -f')

@task
def elastic():
    local('sudo docker-compose build elasticsearch')
    local('sudo docker-compose up -d elasticsearch')

@task
def mysql():
    local('sudo docker-compose build mysql')
    local('sudo docker-compose up -d mysql')
