### Fundamental project for bigger idea.

#### Documentation
Documentation is placed under [docs](https://gitlab.com/acyprods/env/tree/master/docs) directory.

#### Features
1\. Searching in Elasticsearch
* Search by query
* Find single product
* Retrieve product metrics

2\. User management
* User authentication
* User registration
* User sign out
* User details
* Reset password
* Confirm password change
* Change user’s password

#### Technology Stack
##### [Environment](https://gitlab.com/acyprods/env)
+ Vagrant
+ Docker
+ Elasticsearch 6
+ Logstash 6
+ MySQL
+ PostgreSQL

##### [server module](https://gitlab.com/acyprods/server)
+ Java 8
+ Spring Boot 2
+ Spring 5
+ Hibernate 5
+ Lombok

##### [frontend module](https://gitlab.com/acyprods/frontend)
+ nginx
+ JavaScript
+ HTML
+ CSS

##### [fakedatagenerator module](https://gitlab.com/acyprods/fakedatagenerator)
+ python 2.7
+ SQLAlchemy


