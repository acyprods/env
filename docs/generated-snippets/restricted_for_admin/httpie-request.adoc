[source,bash]
----
$ http GET 'http://localhost:8080/api/admin' \
    'Authorization:eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbnRlc3QiLCJleHAiOjE1MjY5Mzg5NjZ9.GzSF2tAku7hqu_jTqg8V5mnCFNmqKxda8vBy0RmIioegXHNmmDQvWbrdcXhXzHwEFU-AAslUJDLCwVEojDKZNA'
----