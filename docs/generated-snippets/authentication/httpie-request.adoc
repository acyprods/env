[source,bash]
----
$ echo '{ "username": "somenewuser", "password": "somepassword"}' | http POST 'http://localhost:8080/api/auth' \
    'Content-Type:application/json'
----