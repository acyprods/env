[source,bash]
----
$ curl 'http://localhost:8080/api/auth' -i -X POST \
    -H 'Content-Type: application/json' \
    -d '{ "username": "somenewuser", "password": "somepassword"}'
----