[source,bash]
----
$ echo '{ "message": "This is an example message.", "contact": "contact@email.com"}' | http POST 'http://localhost:8080/api/leavemessage' \
    'Content-Type:application/json'
----