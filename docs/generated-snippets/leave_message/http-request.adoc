[source,http,options="nowrap"]
----
POST /api/leavemessage HTTP/1.1
Content-Type: application/json
Host: localhost:8080
Content-Length: 75

{ "message": "This is an example message.", "contact": "contact@email.com"}
----