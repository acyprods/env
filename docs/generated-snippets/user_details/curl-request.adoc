[source,bash]
----
$ curl 'http://localhost:8080/api/user/details' -i -X GET \
    -H 'Authorization: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzb21lbmV3dXNlciIsImV4cCI6MTUyNjkzODk2OH0.SRi6kSOcd9u0UjHXQt8Qg_QgQSEzCBoJIMz-c_uuNCeR9p-mqvoxkbXm89Ud-X45kUA5fGBvKolt-_ZHD3_YZg'
----