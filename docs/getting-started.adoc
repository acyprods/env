= Getting started
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 4
:sectlinks:
:operation-curl-request-title: Example request
:operation-http-response-title: Example response

This is a beta tutorial and can contains bugs.

[[paths]]
= 1. First steps

Clone repository

    git clone git@gitlab.com:acyprods/env.git acyprods
    cd acyprods
    git submodule update --init --recursive

Run vagrant

    vagrant up

Enter to vagrant

    vagrant ssh

Run docker containers:

    fab build up

TIP: Note! Every `fab` command must be run from `/vagrant` directory.

[[paths-generate_fake_data]]
== 1.1 Generate fake data
Go to module directory

    cd fakedatagenerator

Install requirements, you can use link:https://virtualenv.pypa.io/en/stable[Virtualenv]

    pip install -r requirements.txt

Run generate.py,
by default will be generate 49 tables with random number of products between 400 to 1000 records and save it to PostgreSQL database.

    python generate.py

Generate configs for Logstash

    python config_generator.py

Generate metrics. Data will be saved in Elasticsearch.

    python generate_metrics.py

Move generated configs to `acyprods/logstash/pipeline`

    cp product_shop_idxxxx.conf acyprods/logstash/pipeline


[[paths-logstash]]
== 1.2 Transfer data do Elasticsearch using Logstash

If configuration is in `pipeline` directory, reload docker. Data from PostgreSQL will be transfer to Elasticsearch.
You can see docker logs using `fab logs`.

    fab down build up

When transfer will be finished you can turn off logstash container:

    docker kill logstash

[[paths-server_module]]
== 1.3 Server module

Run with tests:

    mvn clean install && mvn spring-boot:run -pl runner

Run without tests:

    mvn clean install -DskipTests && mvn spring-boot:run -pl runner

When server module works is available under `localhost:8080`.

<<api-guide.adoc#, API GUIDE>>
