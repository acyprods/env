import os
from fabric.api import *
import fabtasks.docker.compose

env.cwd = os.path.dirname(os.path.realpath(__file__))
env.disable_known_hosts = True

@task
def build():
    execute(fabtasks.docker.compose.build)

@task()
def up():
    execute(fabtasks.docker.compose.up)

@task
def down():
    execute(fabtasks.docker.compose.down)

@task
def logs():
    execute(fabtasks.docker.compose.logs)

@task
def elastic():
    execute(fabtasks.docker.compose.elastic)

@task
def mysql():
    execute(fabtasks.docker.compose.mysql)
